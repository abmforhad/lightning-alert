﻿using System.Collections.Generic;
using WeatherDetectionSystem.Core.Assets;

namespace WeatherDetectionSystem.Test.Builders
{
    public class AssetBuilder
    {
        private string _assetName = "Raynor Avenue";
        private string _quadKey = "test quad key";
        private string _assetOwner = "test owner";

        public AssetBuilder WithAssetName(string value)
        {
            _assetName = value;
            return this;
        }

        public AssetBuilder WithQuadKey(string value)
        {
            _quadKey = value;
            return this;
        }

        public AssetBuilder WithAssetOwner(string value)
        {
            _assetOwner = value;
            return this;
        }

        public IDictionary<string, AssetsWithAlertHistory> Build()
        {
            Asset myassert = new Asset
            {
                AssetName = _assetName,
                QuadKey = _quadKey,
                AssetOwner = _assetOwner
            };

            List<Asset> AuthorList = new List<Asset>();
            AuthorList.Add(myassert);

            return new Dictionary<string, AssetsWithAlertHistory>()
                    {
                        { _quadKey, new AssetsWithAlertHistory { AlreadyAlerted = false, Assets =AuthorList } }
                    };
        }
    }
}


