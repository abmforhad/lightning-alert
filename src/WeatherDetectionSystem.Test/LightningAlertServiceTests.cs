using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Weather;
using WeatherDetectionSystem.Core.Assets;
using WeatherDetectionSystem.Core.LightningAlerts;
using WeatherDetectionSystem.Core.LightningStrikes;
using WeatherDetectionSystem.Test.Builders;

namespace WeatherDetectionSystem.Test
{
    [TestClass]
    public class LightningAlertServiceTests
    {
        [TestMethod]
        public async Task WhenLightningJsonMatchesWithAsset_FetchingDistinctLightningAlert_ShouldReturnCorrectAsset()
        {
            var mockAsset = new AssetBuilder().WithQuadKey("023112320003").Build();

            QuadKeyConverter quadKeyConverter = new QuadKeyConverter();
            var mockAlertService = new Mock<IAssetService>();
            mockAlertService
                .Setup(x => x.GetAssetsMapperAsync())
                .ReturnsAsync(
                    mockAsset
                );

            ILightningAlertService lightningAlertService = new LightningAlertService(quadKeyConverter, mockAlertService.Object);

            string lightningStrikeJson = "{\"flashType\":1,\"strikeTime\":1446760902976,\"latitude\":32.9905308,\"longitude\":-98.34038,\"peakAmps\":1700,\"reserved\":\"000\",\"icHeight\":17229,\"receivedTime\":1446760915185,\"numberOfSensors\":14,\"multiplicity\":8}";

            var actualResult = await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);

            Assert.AreEqual(1, actualResult.Count());
        }

        [TestMethod]
        public async Task WhenLightningJsonMatchesWithAsset_FetchingLightningAlertMultipleTimes_ShouldReturnCorrectAssetOnlyForFirstCall()
        {
            var mockAsset = new AssetBuilder().WithQuadKey("023112320003").Build();

            QuadKeyConverter quadKeyConverter = new QuadKeyConverter();
            var mockAlertService = new Mock<IAssetService>();
            mockAlertService
                .Setup(x => x.GetAssetsMapperAsync())
                .ReturnsAsync(
                    mockAsset
                );

            ILightningAlertService lightningAlertService = new LightningAlertService(quadKeyConverter, mockAlertService.Object);

            string lightningStrikeJson = "{\"flashType\":1,\"strikeTime\":1446760902976,\"latitude\":32.9905308,\"longitude\":-98.34038,\"peakAmps\":1700,\"reserved\":\"000\",\"icHeight\":17229,\"receivedTime\":1446760915185,\"numberOfSensors\":14,\"multiplicity\":8}";

            var actualResult = await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);

            Assert.AreEqual(1, actualResult.Count());

            var duplicateActualResult = await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);

            Assert.AreEqual(0, duplicateActualResult.Count());
        }

        [TestMethod]
        public async Task WhenLightningJsonDoesNotMatchWithAsset_FetchingLightningAlert_ShouldReturnEmptyList()
        {
            var mockAsset = new AssetBuilder().WithQuadKey("023112320093").Build();

            QuadKeyConverter quadKeyConverter = new QuadKeyConverter();
            var mockAlertService = new Mock<IAssetService>();
            mockAlertService
                .Setup(x => x.GetAssetsMapperAsync())
                .ReturnsAsync(
                    mockAsset
                );

            ILightningAlertService lightningAlertService = new LightningAlertService(quadKeyConverter, mockAlertService.Object);

            string lightningStrikeJson = "{\"flashType\":1,\"strikeTime\":1446760902976,\"latitude\":32.9905308,\"longitude\":-98.34038,\"peakAmps\":1700,\"reserved\":\"000\",\"icHeight\":17229,\"receivedTime\":1446760915185,\"numberOfSensors\":14,\"multiplicity\":8}";

            var actualResult = await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);

            Assert.AreEqual(0, actualResult.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidLightningStrikeException))]
        public async Task WheLightningStrikeDataIsInvalid_FetchingLightningAlert_ShouldThrowInvalidLightningStrikeException()
        {
            var mockAsset = new AssetBuilder().WithQuadKey("023112320003").Build();

            QuadKeyConverter quadKeyConverter = new QuadKeyConverter();
            var mockAlertService = new Mock<IAssetService>();
            mockAlertService
                .Setup(x => x.GetAssetsMapperAsync())
                .ReturnsAsync(
                    mockAsset
                );

            ILightningAlertService lightningAlertService = new LightningAlertService(quadKeyConverter, mockAlertService.Object);
            string lightningStrikeJson = "\"flashType\":1,\"strikeTime\":1446760902510,\"latitude\":8.7020156,\"longitude\":-12.2736188,\"peakAmps\":3034,\"reserved\":\"000\",\"icHeight\":11829,\"receivedTime\":1446760915181,\"numberOfSensors\":6,\"multiplicity\":1}";

            await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);
        }
    }
}
