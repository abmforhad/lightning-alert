﻿using System.Threading.Tasks;

namespace WeatherDetectionSystem.Core.Interfaces
{
    public interface IDatabase
    {
        Task<T> GetDataAsync<T>();
    }
}