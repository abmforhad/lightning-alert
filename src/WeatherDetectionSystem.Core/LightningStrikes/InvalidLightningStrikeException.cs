﻿using System;

namespace WeatherDetectionSystem.Core.LightningStrikes
{
    public class InvalidLightningStrikeException : Exception
    {
        public InvalidLightningStrikeException(string message) : base(message)
        {
            
        }
    }
}