﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WeatherDetectionSystem.Core.LightningStrikes
{
    public class LightningStrikeService : ILightningStrikeService
    {
        public async Task<IEnumerable<LightningStrike>> GetLightningStrikesAsync()
        {
            using StreamReader r = new StreamReader("Data/lightning.json");
            var lines = (await r.ReadToEndAsync()).Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var strikes = lines.Select(JsonConvert.DeserializeObject<LightningStrike>).ToList();
            return strikes;
        }
    }
}