﻿namespace WeatherDetectionSystem.Core.LightningStrikes
{
    public enum FlashType
    {
        CloudToGround = 0,
        CloudToCloud = 1,
        HeartBeat = 9
    }
}