﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WeatherDetectionSystem.Core.LightningStrikes
{
    public interface ILightningStrikeService
    {
        public Task<IEnumerable<LightningStrike>> GetLightningStrikesAsync();
    }
}