﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WeatherDetectionSystem.Core.Assets
{
    public interface IAssetService
    {
        Task<IDictionary<string, AssetsWithAlertHistory>> GetAssetsMapperAsync();
    }
}