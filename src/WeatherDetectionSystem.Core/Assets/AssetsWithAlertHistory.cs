﻿using System.Collections.Generic;

namespace WeatherDetectionSystem.Core.Assets
{
    public class AssetsWithAlertHistory
    {
        public IEnumerable<Asset> Assets { get; set; }
        public bool AlreadyAlerted { get; set; }
    }
}