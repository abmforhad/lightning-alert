﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherDetectionSystem.Core.Interfaces;

namespace WeatherDetectionSystem.Core.Assets
{
    public class AssetService : IAssetService
    {
        private readonly IDatabase _database;
        private IDictionary<string, AssetsWithAlertHistory> _assetsMapper;
        public AssetService(IDatabase database)
        {
            _database = database;
        }

        private async Task<IEnumerable<Asset>> GetAssetsAsync()
        {
            var allAssets = (await _database.GetDataAsync<IEnumerable<Asset>>()).ToList();
            return allAssets;
        }

        public async Task<IDictionary<string, AssetsWithAlertHistory>> GetAssetsMapperAsync()
        {
            if (_assetsMapper != null && _assetsMapper.Any())
            {
                return _assetsMapper;
            }

            var allAssets = (await GetAssetsAsync()).ToList();
           return _assetsMapper = allAssets.GroupBy(a => a.QuadKey,
                    (quadKey, g) => new
                        {Key = quadKey, AssetsWithMapper = new AssetsWithAlertHistory {Assets = g, AlreadyAlerted = false}})
                .ToDictionary(kv => kv.Key, kv => kv.AssetsWithMapper);
       
        }
    }
}