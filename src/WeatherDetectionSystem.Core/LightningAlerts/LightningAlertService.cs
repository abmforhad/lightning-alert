﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Weather;
using WeatherDetectionSystem.Core.Assets;
using WeatherDetectionSystem.Core.LightningStrikes;

namespace WeatherDetectionSystem.Core.LightningAlerts
{
    public class LightningAlertService : ILightningAlertService
    {
        private const int ZoomLevel = 12;
        private readonly QuadKeyConverter _quadKeyConverter;
        private readonly IAssetService _assetService;

        public LightningAlertService(QuadKeyConverter quadKeyConverter, IAssetService assetService)
        {
            _quadKeyConverter = quadKeyConverter;
            _assetService = assetService;
        }

        public async Task<IEnumerable<Asset>> GetLightningAlertedAssetsAsync(LightningStrike lightningStrike)
        {
            if (lightningStrike.FlashType == FlashType.HeartBeat)
            {
                return new List<Asset>();
            }
            var assetsMapper = await _assetService.GetAssetsMapperAsync();
            var quadKey =
                _quadKeyConverter.ConvertLatLongToQuadKey(lightningStrike.Latitude, lightningStrike.Longitude, ZoomLevel);
            
            assetsMapper.TryGetValue(quadKey, out var assetsWithAlertHistory);
            if (assetsWithAlertHistory == null || assetsWithAlertHistory.AlreadyAlerted)
            {
                return new List<Asset>();
            }
            assetsWithAlertHistory.AlreadyAlerted = true;
            assetsMapper[quadKey] = assetsWithAlertHistory;

            return assetsWithAlertHistory.Assets.ToList();
        }

        public async Task<IEnumerable<Asset>> GetLightningAlertedAssetsAsync(string lightningStrikeJson)
        {
            try
            {
                var lightningStrike = JsonConvert.DeserializeObject<LightningStrike>(lightningStrikeJson);
                return await GetLightningAlertedAssetsAsync(lightningStrike);
            }
            catch (JsonException e)
            {
                throw new InvalidLightningStrikeException(e.Message);
            }
        }
    }
}