﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherDetectionSystem.Core.Assets;
using WeatherDetectionSystem.Core.LightningStrikes;

namespace WeatherDetectionSystem.Core.LightningAlerts
{
    public interface ILightningAlertService
    {
        Task<IEnumerable<Asset>> GetLightningAlertedAssetsAsync(LightningStrike lightningStrike);
        Task<IEnumerable<Asset>> GetLightningAlertedAssetsAsync(string lightningStrikeJson);
    }
}
