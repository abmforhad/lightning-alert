﻿using System;
using System.Drawing;
using System.Text;

namespace Weather
{
    public class QuadKeyConverter
    {
        private const double MinLatitude = -85.05112878;
        private const double MaxLatitude = 85.05112878;
        private const double MinLongitude = -180;
        private const double MaxLongitude = 180;
        double Clip(double n, double minValue, double maxValue)
        {
            return Math.Min(Math.Max(n, minValue), maxValue);
        }
        public static uint MapSize(int levelOfDetail)
        {
            return (uint)256 << levelOfDetail;
        }
        public string ConvertLatLongToQuadKey(double latitude, double longitude, int levelOfDetail)
        {
            Point pixel = ConvertLatLongToPixelXY(latitude, longitude, levelOfDetail);
            Point tile = ConvertPixelXYToTileXY(pixel);
            string quadKey = ConvertTileXYToQuadKey(tile, levelOfDetail);
            return quadKey;
        }
        Point ConvertLatLongToPixelXY(double latitude, double longitude, int levelOfDetail)
        {
            latitude = Clip(latitude, MinLatitude, MaxLatitude);
            longitude = Clip(longitude, MinLongitude, MaxLongitude);

            double x = (longitude + 180) / 360;
            double sinLatitude = Math.Sin(latitude * Math.PI / 180);
            double y = 0.5 - Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);

            uint mapSize = MapSize(levelOfDetail);
            int pixelX = (int)Clip(x * mapSize + 0.5, 0, mapSize - 1);
            int pixelY = (int)Clip(y * mapSize + 0.5, 0, mapSize - 1);
            return new Point(pixelX, pixelY);
        }
        Point ConvertPixelXYToTileXY(Point pixelXY)
        {
            int tileX = pixelXY.X / 256;
            int tileY = pixelXY.Y / 256;
            return new Point(tileX, tileY);
        }
        string ConvertTileXYToQuadKey(Point tileXY, int levelOfDetail)
        {
            StringBuilder quadKey = new StringBuilder();
            for (int i = levelOfDetail; i > 0; i--)
            {
                char digit = '0';
                int mask = 1 << (i - 1);
                if ((tileXY.X & mask) != 0)
                {
                    digit++;
                }
                if ((tileXY.Y & mask) != 0)
                {
                    digit++;
                    digit++;
                }
                quadKey.Append(digit);
            }
            return quadKey.ToString();
        }
    }
}
