﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Weather;
using WeatherDetectionSystem.Core.Assets;
using WeatherDetectionSystem.Core.Extensions;
using WeatherDetectionSystem.Core.Interfaces;
using WeatherDetectionSystem.Core.LightningAlerts;
using WeatherDetectionSystem.Core.LightningStrikes;
using WeatherDetectionSystem.Infrastructure.Data;

namespace WeatherDetectionSystem.ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IDatabase database = new WeatherDetectionSystemAssetFileDatabase();
            QuadKeyConverter quadKeyConverter = new QuadKeyConverter();
            IAssetService assetService = new AssetService(database);
            ILightningAlertService lightningAlertService =
                new LightningAlertService(quadKeyConverter, assetService);

            Console.WriteLine("Please provide input (one lightning strike line as a JSON object) and press enter:");

            string lightningStrikeJson;
            while ((lightningStrikeJson = Console.ReadLine()) != null)
            {
                try
                {
                    var distinctLightningAlertedAssets =
                        await lightningAlertService.GetLightningAlertedAssetsAsync(lightningStrikeJson);
                    PrintLightningAlert(distinctLightningAlertedAssets);
                }
                catch (InvalidLightningStrikeException)
                {
                    Console.WriteLine("Invalid data");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            // below code will  read all the lightning strike one by one form the json file and matches the data against a source of assets to produce all the alerts.

            /*
           ILightningStrikeService lightningStrikeReader = new LightningStrikeService();
           var strikes = await lightningStrikeReader.GetLightningStrikesAsync();

           await strikes.VisitAsync(async strike =>
           {
               PrintLightningAlert(await lightningAlertService.GetLightningAlertedAssetsAsync(strike));
               return true;
           });
            */
        }
        static void PrintLightningAlert(IEnumerable<Asset> alertedAssets)
        {
            alertedAssets.Visit(asset =>
            {
                Console.WriteLine($"lightning alert for {asset.AssetOwner}:{asset.AssetName}");
            });
        }
    }
}
