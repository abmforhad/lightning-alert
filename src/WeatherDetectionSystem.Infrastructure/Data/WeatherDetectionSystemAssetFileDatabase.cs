﻿using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WeatherDetectionSystem.Core.Interfaces;

namespace WeatherDetectionSystem.Infrastructure.Data
{
    public class WeatherDetectionSystemAssetFileDatabase : IDatabase
    {
        private const string Path = "Data/assets.json";
        public async Task<T> GetDataAsync<T>()
        {
            using StreamReader r = new StreamReader(Path);
            string json = await r.ReadToEndAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}