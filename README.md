# Weather Detection System (lightning-alert )#

This Weather Detection System application reads lightning events data as a stream from standard input (one lightning strike per line as a JSON object, and matches that data against a source of assets (also in JSON format) to produce an alert.

### Architecture and Tecnology ###
*  This application follow the principles of Clean Architecture
*  .NET Core 3.1
*  MSTest 
*  Moc
*  With SonarLint, I try to ensure the standards of the code

### Getting Started ###

* Set WeatherDetectionSystem.ConsoleApp as Startup project
* If there any file path issue, set lightning.json and assets.json (under Infrastructure project) file property copy always.  

### Important Notes ###

* This console application expect input which is one line  lightning strike as a JSON object
* To view all alerts without human interaction, please comment out line 24-43 and uncomment  line 47-56 in Program.cs (ConsoleApp)

### Questions answer ###

* Time complexity is O(1), if a strike has occurred for a particular asset
* To improve the performance, we can implement MemoryCache
* if the database  is very big and more users in production then we can think about distributed cache system like redis.


